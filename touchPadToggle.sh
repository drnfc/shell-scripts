#!/usr/bin/env sh

export TOUCHID=$(xinput list | grep Touch | awk '{print $6}' | sed 's/id=//')

export IS_TOUCH_ENABLED=$(xinput --list-props $TOUCHID | \
    grep "Enabled" | \
    awk '{print $4}')

if [ "$IS_TOUCH_ENABLED" == 0 ];
    then
        xinput --enable "$TOUCHID"
    else
        xinput --disable "$TOUCHID"
fi
